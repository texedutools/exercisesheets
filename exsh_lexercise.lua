exsh_texprintlines = function(s)
  for line in s:gmatch("[^\n]*") do
    tex.print(line)
  end
end


solution = function(sol)
  if type(sol) ~= "string" then
    local solt = sol
    sol = "["
    if solt.idea then
      sol = sol .. "idea,"
    end
    if solt.name then
      sol = sol .. "strings/solution={"..solt.name.."},"
    end
    sol = sol .. "] " .. (solt.text or "") 
  end
  return [[\begin{solution}]]..sol..[[\end{solution}]]
end

subexercise = function(se)
  local pts,p,bp = ""
  local sols = ""
  if type(se) ~= "string" then
    local set = se
    local po = ""
    if set.pointoptions then
      po = po .. set.pointoptions
    end
    p,bp = set.points,set.bonuspoints 
    if  p and bp then
      pts = [[\points[]] .. po .. "]{" .. set.points .. "+" ..
        set.bonuspoints .. "}"
    elseif bp then
      pts = [[\points[bonus,]] .. po .. "]{" .. set.bonuspoints .. "}"
    elseif p then
      pts = [[\points[]] .. po .. "]{" .. set.points .. "}"
    end
    
    if set.solution then
      sols = sols .. solution(set.solution)
    end
    if set.altsolutions then
      for _,sol in ipairs(set.altsolutions) do
        sols = sols .. solution(sol)
      end
    end
    
    se = "["
    if set.options then
      se = se .. set.options
    end
    se = se .. "]{" .. (set.task or "") .. "}"
  else
    se = "{" .. se .. "}"
  end
  return [[\subtask]]..se .. pts .. sols, p, bp
end

exercise = function(ex)
  local subex = ""
  local pts,bpts = 0,0
  if ex.subexercises then
    subex =  [[\begin{subtasks}]]
    for _,se in ipairs(ex.subexercises) do
      local s,p,bp = subexercise(se)
      subex = subex .. s
      pts = pts + (p or 0)
      bpts = bpts + (bp or 0)
    end
    subex = subex .. [[\end{subtasks}]]
  end
  if ex.points and ex.points == "sum" then
    if  pts > 0  and bpts > 0 then
      pts = pts .. "+" .. bpts
    elseif bpts > 0 then --and pts = 0
      pts = bpts
      ex.options = (ex.options or "") .. ",bonus"
    --else bpts = 0 thus pts is all points 
    end
  else
    pts = ex.points
  end

  local ece = [[\begin{exercise}[]]
  if ex.firstline then
    ece = ece .. [[firstline={]] .. ex.firstline .. [[},]]
  end
  if pts then
    ece = ece .. [[points={]] .. pts .. [[},]]
  end
  if ex.name then
    ece = ece .. [[name={]] .. ex.name .. [[},]]
  end
  if ex.options then
    ece = ece .. ex.options .. ","
  end
  ece = ece .. "]{}"
  if ex.task then
    ece = ece .. [[\begin{maintask}]]  .. ex.task 
      .. [[\end{maintask}]]
  end
  if ex.solution then
    ece = ece .. solution(ex.solution)
  end
  if ex.altsolutions then
    for _,sol in ipairs(ex.altsolutions) do
      ece = ece .. solution(sol)
    end
  end
  
  ece = ece .. subex

  ece = ece .. "\n" .. [[\end{exercise}]]
  return ece
end

fragileframed = function(s)
  local frames,p,pn,n,i = {},1,1,#s,0
  pn = s:find([[\newframe]],p) or n+1
  while p < n do
    i = i+1
    frames[i] = [[
      \begin{frame}[t,fragile] 
      \solutiontitle[\textwidth]
      ]] .. s:sub(p,pn-1) .. [[
      \end{frame}
    ]] 
    p = pn + 9
    pn = s:find([[\newframe]],p) or n+1
  end
  return table.concat(frames,"")
end

